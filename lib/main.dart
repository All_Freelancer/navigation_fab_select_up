import 'package:flutter/material.dart';
import 'package:navigation_fab_select_up/src/ui/widgets/curved_navigation_bar.dart';


//********************************************* CLASS MAIN ********************************************* */
void main() => runApp(MaterialApp(home: BottomNavBar()));


//********************************************* CLASS BOTTOM NAV BAR ********************************** */
class BottomNavBar extends StatefulWidget {
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

//******************************************* STATE BOTTOM NAV BAR *************************************** */
class _BottomNavBarState extends State<BottomNavBar> {
  int _page = 0;
  final double sizeImage = 28; 
  GlobalKey _bottomNavigationKey = GlobalKey();

  //************************************** ROOT WIDGET ************************************* */
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,        

        //::::::::::::::::::::::::::::::::::: NAVIGATION BAR :::::::::::::::::::::::
        bottomNavigationBar: /*Container(
          height: 65.0,
          */
          //child: 
          CurvedNavigationBar(     // CALL CLASS curved_navigation_bar 
            key: _bottomNavigationKey,
            index: 0,
            height: 75.0,

            items: <Widget>[
              /*
              SizedBox(
                height: 30,
                child: 
                   Image.asset("assets/icons/icon_select-28.png"), 
              ),
              */
              Image.asset("assets/icons/icon_select-28.png", height: sizeImage,), 
              Icon(Icons.add, size: sizeImage),
              Icon(Icons.list, size: sizeImage),
              Icon(Icons.compare_arrows, size: sizeImage),
              Icon(Icons.call_split, size: sizeImage),
              Icon(Icons.perm_identity, size: sizeImage),
            ],

            color: Colors.white,
            buttonBackgroundColor: Colors.white,
            backgroundColor: Colors.blueAccent,
            animationCurve: Curves.easeInOut,
            animationDuration: Duration(milliseconds: 600),
            
            onTap: (index) {
              setState(() {
                _page = index;
              });
            },
         //),
        ),
        
        //::::::::::::::::::::::::::::::::::: BODY ::::::::::::::::::::::::::::::::
        body: Container(
          color: Colors.blueAccent,
          child: Center(
            child: Column(
              children: <Widget>[
                Text(_page.toString(), textScaleFactor: 10.0),
                RaisedButton(
                  child: Text('Go To Page of index 1'),
                  onPressed: () {
                    final CurvedNavigationBarState navBarState =
                        _bottomNavigationKey.currentState;
                    navBarState.setPage(1);
                  },
                )
              ],
            ),
          ),
        ));
  }
}
//********************************************************************************************************************* */
