import 'package:flutter/material.dart';

//**************************************************** CLASS NAV CUSTOM PAINTER ************************************* */
class NavCustomPainter extends CustomPainter {
  double loc;
  double s;
  Color color;
  TextDirection textDirection;

  //*************************************** CONSTRUCT ********************************* */
  NavCustomPainter(
      double startingLoc, int itemsLength, this.color, this.textDirection) {
        
    final span = 1.0 / itemsLength;
    s = 0.2;
    double l = startingLoc + (span - s) / 2;
    loc = textDirection == TextDirection.rtl ? 0.8 - l : l;
  }

  //************************************** ROOT WIDGET ******************************* */
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    final path = Path()
      ..moveTo(0, 0)
      ..lineTo((loc - 0.1) * size.width, 0)
      ..cubicTo(
        (loc + s * 0.20) * size.width,
        size.height * 0.05,
        loc * size.width,
        size.height * 0.65,
        (loc + s * 0.50) * size.width,
        size.height * 0.65,             // SIZE 0.60
      )
      ..cubicTo(
        (loc + s) * size.width,
        size.height * 0.65,
        (loc + s - s * 0.20) * size.width,
        size.height * 0.05,
        (loc + s + 0.1) * size.width,
        0,
      )
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height)
      ..lineTo(0, size.height)
      ..close();
    canvas.drawPath(path, paint);
  }

  //************************************** METHOD SHOULD REPAINT ******************************* */
  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return this != oldDelegate;
  }
}
//**************************************************************************************************************************** */
